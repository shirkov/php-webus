<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Article extends Model
{
    protected $primaryKey = 'articol_id';

    // какие поля могут быть заполнены выполняя Article::create(), Article::update()
    protected $fillable = [
        'title',
        'description',
        'text',
        'image',
        'send_to_admin_email'
    ];

    // function for 'set' or 'delete'
    public function setImage($value = null){
        $this->image = $value;
        $this->save();
    }
}
