@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <h1 class="display-4">Article: {{$article->articol_id}}</h1>

                  <div class="response-ajax">

                  </div>

                <form action="" method="POST" id="articleForm" enctype="multipart/form-data">
                    @csrf
                    <div class="form-group">
                        <label>Title</label>
                        <input type="text" name="title" class="form-control" value="{{ $article->title}}">
                    </div>
                    <div class="form-group">
                        <label>Description</label>
                        <input type="text" name="description" class="form-control" value="{{ old('description', $article->description) }}">
                    </div>
                    <div class="form-group">
                        <label>Text</label>
                        <textarea name="text" class="form-control">{{$article->text}}</textarea>
                    </div>

                    @if ($article->image)
                        <div class="form-group img-group">
                            <label>Image</label>
                            <div>
                                <img src="{{$article->image}}" alt="" width="200px">
                                <a href="{{$article->articol_id}}/deleteimg" class="btn btn-danger remove-article-img-js">Delete</a>
                            </div>
                        </div>
                    @endif

                    <div class="form-group upload-file-block @if ($article->image) hidden @endif">
                        <label>Upload new image</label>
                        <input name="inputFile" type="file" class="form-control file-input">
                    </div>

                    <div class="checkbox">
                        <label>
                            <input type="checkbox" name="send_mail" @if ($article->send_to_admin_email) checked @endif value=""> Send email to admin
                        </label>
                    </div>
                    <button type="submit" class="btn btn-primary">Submit</button>

                    <a href="{{route('article.index')}}" class="btn btn-secondary">Back</a>
                </form>
            </div>
        </div>
    </div>
@endsection
