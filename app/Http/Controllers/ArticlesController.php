<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Article;
use Illuminate\Support\Carbon;
use App\Mail\NewArticleMail;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use App\Jobs\EmailSent;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

class ArticlesController extends Controller
{
    public function index(Request $request)
    {

//        $articlesWhereIn = Article::whereIn('articol_id', ['55', '56', '57'])->get();
//        $articlesLimit = Article::where('articol_id', 55)->limit('id')->get();
        $articles = Article::latest()->paginate(4);

        if ($request->ajax()) {
            return view('loop', ['articles' => $articles])->render(); // need for ajax
        }

        return view('index', [
            'articles' => $articles
        ]);
    }

    public function show($id)
    {

        $article = Article::findOrFail($id);

        return view('single-article', ['article'=> $article]);
    }

    public function add()
    {
        return view('article-add');
    }

    public function edit($id)
    {
        $article = Article::findOrFail($id);

        return view('article-edit', ['article'=> $article]);
    }

    public function deleteImg($id)
    {
        $article = Article::findOrFail($id);

        $article->setImage();

        return response([
            'message' => 'Img was deleted'
        ]);
    }

    public function update(Request $request, $id)
    {
        // validate throw make, to not reload page
        $validator = Validator::make($request->all(), [
            'title' => 'required|max:270|min:2',
            'description' => 'max:100|min:2',
            'text' => 'required|max:2500',
        ]);

        if($validator->fails()){
            return response([
                'errors' => $validator->errors(),
                'message' => 'Validator error!'
            ]);
        }else{
            $article = Article::findOrFail($id);

//            dd($article->image);

            $imgPath = $article->image;

            if ($request->hasFile('inputFile')) {
                $imgPath = Storage::disk('public')->put('image', $request->inputFile);
                $imgPath = sprintf('/storage/%s', $imgPath);
            }

            $update = $article->update([
                'title' => $request->title,
                'description' => $request->description,
                'text' => $request->text,
                'image' => $imgPath,
            ]);

            if  ($request->has('send_mail')){
                EmailSent::dispatch($article);
            }

            return response([
                'errors' => false,
                'message' => 'Article has been updated!',
                'request' => $request->all()
            ]);
        }

    }

    public function save(Request $request)
    {
        // Docs: https://laravel.com/docs/5.7/
        $imgPath = null;

        if ($request->hasFile('inputFile')) {
            $imgPath = Storage::disk('public')->put('image', $request->inputFile);
            $imgPath = sprintf('/storage/%s', $imgPath);
        }

        $request->validate([
            'title' => 'required|max:270|min:2',
            'description' => 'max:100',
            'text' => 'required|max:2500',
        ]);

        $article = Article::create([
            'title' => $request->title,
            'description' => $request->description,
            'text' => $request->text,
            'image' => $imgPath,
        ]);

        if  ($request->has('send_mail')){
            EmailSent::dispatch($article);
            //  sent mail without queue
            //  Mail::to('admin@site.com')->send(new NewArticleMail($article));
        }

        $request->session()->flash('mail_msg', 'Message was successfully added!');

        return redirect()->route('article.index');
    }

    public function delete($id)
    {
        Article::destroy($id);

        // use axios instead
        // $request->session()->flash('mail_msg', 'Article was successfully deleted!');

        return response([
            'message' => sprintf('Article with id:%d was successfully deleted!', $id)
        ]);
    }
}
