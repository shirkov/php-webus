@foreach($articles as $article)
    <tr>
        <td>{{ $article->articol_id }}</td>
        <td>{{ $article->title }}</td>
        <td>{{ \Illuminate\Support\Carbon::parse($article->created_at)->format('d-M-Y') }}</td>
        <td>
            @auth
                <a href="{{ route('article.edit', ['id' => $article->articol_id]) }}" class="btn btn-primary">Edit</a>
            @endauth
            <a href="{{ route('article.delete', ['id' => $article->articol_id]) }}" class="btn btn-danger delete-article-js">
                Delete
            </a>
            <a href="{{ route('article.show', ['id' => $article->articol_id]) }}" class="btn btn-warning">
                Show
            </a>
        </td>
    </tr>
@endforeach
