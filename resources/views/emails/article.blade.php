@component('mail::message')
# Article created!

A new article has been created or edited!


@if($article->title)
 # Title: {{ $article->title }}
@endif

@if($article->text)
    # Text: {{ $article->text }}
@endif

@if($article->description)
    # Description: {{ $article->description }}
@endif

@if($article->image)
    # Image: {{ $article->image }}
@endif


@component('mail::button', ['url' => route('article.show', ['id'=> $article->articol_id] ), 'color' => 'green'])
View Order
@endcomponent


Thanks,<br>
{{ config('app.name') }}
@endcomponent
