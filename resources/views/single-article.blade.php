@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <h1 class="display-4">Article: {{$article->articol_id}}</h1>

                <form action="" method="POST">
                    @csrf
                    <div class="form-group">
                        <label>Title</label>
                        <input readonly type="text" name="title" class="form-control" value="{{$article->title}}">
                    </div>
                    <div class="form-group">
                        <label>Description</label>
                        <input readonly type="text" name="description" class="form-control" value="{{$article->description}}">
                    </div>
                    <div class="form-group">
                        <label>Text</label>
                        <textarea readonly name="text" class="form-control">{{$article->text}}</textarea>
                    </div>
                    @if($article->image)
                    <div class="form-group">
                        <label>Image</label>
                        <div>
                            <img src="{{$article->image}}" alt="" width="200px">
                        </div>
                    </div>
                    @endif
                    <a href="{{route('article.index')}}" class="btn btn-secondary">Back</a>
                </form>
            </div>
        </div>
    </div>

@endsection
