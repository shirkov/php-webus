@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">

            <div class="response-ajax">
                @if(Session::has('mail_msg'))
                    <div class="alert alert-success">
                        {{ Session::get('mail_msg') }}
                    </div>
                @endif
            </div>

            <div class="card">
                <div class="card-header">Articles</div>
                <div class="card-body">
                   <table class="table">
                      <thead>
                         <tr>
                           <th>ID</th>
                           <th>Name</th>
                           <th>Created at</th>
                           <th>Actions</th>
                        </tr>
                      </thead>
                      <tbody>
                        @include('loop')
                      </tbody>
                   </table>
                    <br>
                    @auth
                        <div class="text-right">
                            <a href="{{ route('article.add') }}" class="btn btn-success">
                                Add
                            </a>
                        </div>
                    @endauth
                    {{-- pagination --}}
                    <div class="d-flex justify-content-center">
                        {{--{{ $articles->links() }}--}}
                        {{ $articles->onEachSide(3)->links() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
