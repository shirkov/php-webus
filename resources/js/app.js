
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

var serialize = require('form-serialize');

// window.Vue = require('vue');

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// Vue.component('example-component', require('./components/ExampleComponent.vue'));

// const files = require.context('./', true, /\.vue$/i)
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key)))

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

// const app = new Vue({
//     el: '#app'
// });


/**
 * Edit Article ajax request
 */

let form = document.getElementById('articleForm');

if(form !== null){
    form.addEventListener('submit', function(e){
        e.preventDefault();

        var data = serialize(form, { hash: true });
        var inputFile = document.querySelector('.file-input');

        // Docs: https://stackoverflow.com/questions/21044798/how-to-use-formdata-for-ajax-file-upload
        var fd = new FormData(); // to work with file and ajax

        if(inputFile !== null){
            fd.append('inputFile', inputFile.files[0]);
        }

        // prepare formdata array from ajax
        for (var k in data){
            if (typeof data[k] !== 'function') {
                fd.append(k, data[k]);
            }
        }

        var lastSegment = window.location.pathname.split("/").pop()

        axios.post('/articles/edit/'+lastSegment, fd)
            .then(function (response) {
                console.log(response.data);

                let infoBlock = document.getElementsByClassName('response-ajax')[0];

                if(response.data.errors){

                    infoBlock.innerHTML = '';

                    let errorArr = '';

                        for(var index in response.data.errors) {
                            var values = response.data.errors[index];

                            values.forEach(function(element) {
                                errorArr += '<li>'+element+'</li>'
                            });
                        }

                    infoBlock.innerHTML = '<div class="alert alert-danger"><ul>'+errorArr+'</ul></div>';
                } else{
                    infoBlock.innerHTML = '';
                    infoBlock.innerHTML = '<div class="alert alert-success">\n' +
                        '                    '+ response.data.message +'\n' +
                        '                </div>';
                }

            })
            .catch(function (error) {
                console.log(error);
            });
    })
}

/**
 * Delete Article-Img ajax request
 */

// get all btn's
let deleteImgBtn = document.querySelector('.remove-article-img-js');

if(deleteImgBtn !== null){
    deleteImgBtn.addEventListener("click", function(e){
        e.preventDefault();

        axios.post(deleteImgBtn.getAttribute('href'))
            .then(function (response) {
                console.log(response.data);

                // set alert message
                let infoBlock = document.querySelector('.response-ajax');
                let uploadBlock = document.querySelector('.upload-file-block');

                infoBlock.innerHTML = '';

                infoBlock.innerHTML = '<div class="alert alert-success">\n' +
                    '                    '+ response.data.message +'\n' +
                    '                </div>';

                // delete HTML block
                let imgblock = document.querySelector('.img-group');

                // show upload new img block
                if (uploadBlock.classList){
                    uploadBlock.classList.remove('hidden');
                }

                imgblock.innerHTML = '';
            })
            .catch(function (error) {
                console.log(error);
            });
    });
}


/**
 * Delete Article ajax request
 */

// get all btn's
let deleteBtn = document.querySelectorAll('.delete-article-js');

// add function on click
for(var i = 0; i < deleteBtn.length; i++) {
    deleteBtn[i].addEventListener("click", bindClick(i));
}

// btn function click
function bindClick(i) {
    return function(e) {
        e.preventDefault();

        axios.post(deleteBtn[i].getAttribute('href'))
            .then(function (response) {
                console.log(response.data);

                // set alert message
                let infoBlock = document.querySelector('.response-ajax');

                infoBlock.innerHTML = '';

                infoBlock.innerHTML = '<div class="alert alert-success">\n' +
                    '                    '+ response.data.message +'\n' +
                    '                </div>';

                // delete HTML block
                // console.log(deleteBtn[i]);
                let td = deleteBtn[i].parentElement;
                let tr = td.parentElement;

                tr.innerHTML = '';
            })
            .catch(function (error) {
                console.log(error);
            });
    };
}


/**
 * Pagination Ajax
 */

// get all btn's
let paginationBtn = document.querySelectorAll('.page-link');

// add function on click
for(var i = 0; i < paginationBtn.length; i++) {
    paginationBtn[i].addEventListener("click", paginationClick(i));
}

function paginationClick(i) {
    return function(e) {
        e.preventDefault();

        console.log(paginationBtn[i].getAttribute('href'));

        axios.get(paginationBtn[i].getAttribute('href'))
            .then(function (response) {
                console.log(response.data);

                let tbody = document.querySelector('tbody');
                tbody.innerHTML = response.data;
            })
            .catch(function (error) {
                console.log(error);
            });
    };
}
