<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
   return redirect(route('article.index'));
});


Route::prefix('articles')->group(function () {

    // index
    Route::get('/', 'ArticlesController@index')->name('article.index');

    // single
    Route::get('/{id}', 'ArticlesController@show')
        ->where('id', '[0-9]+')
        ->name('article.show');

    // add
    Route::get('/add', 'ArticlesController@add')->middleware('auth')->name('article.add');

    // edit
    Route::get('/edit/{id}', 'ArticlesController@edit')
        ->middleware('auth')
        ->where('id', '[0-9]+')
        ->name('article.edit');

    // edit(post)
    Route::post('/edit/{id}', 'ArticlesController@update')
        ->middleware('auth')
        ->where('id', '[0-9]+')
        ->name('article.edit');

    // edit/id/deleteimg
    Route::post('/edit/{id}/deleteimg', 'ArticlesController@deleteImg')
        ->middleware('auth')
        ->where('id', '[0-9]+');

    //save
    Route::post('/save', 'ArticlesController@save')->name('article.save');

    //delete
    Route::any('/delete/{id}', 'ArticlesController@delete')
        ->where('id', '[0-9]+')
        ->name('article.delete');
});

// mail test
Route::get('/mail', function () {
    $article = App\Article::find(2);

    return new App\Mail\NewArticleMail($article);
});

Auth::routes();

Route::get('/home', function(){
    return redirect(route('article.index'));
})->name('home');
